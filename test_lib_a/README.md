# test_lib_a

[![CI Status](https://img.shields.io/travis/tudoan/test_lib_a.svg?style=flat)](https://travis-ci.org/tudoan/test_lib_a)
[![Version](https://img.shields.io/cocoapods/v/test_lib_a.svg?style=flat)](https://cocoapods.org/pods/test_lib_a)
[![License](https://img.shields.io/cocoapods/l/test_lib_a.svg?style=flat)](https://cocoapods.org/pods/test_lib_a)
[![Platform](https://img.shields.io/cocoapods/p/test_lib_a.svg?style=flat)](https://cocoapods.org/pods/test_lib_a)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

test_lib_a is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'test_lib_a'
```

## Author

tudoan, datu1681996@gmail.com

## License

test_lib_a is available under the MIT license. See the LICENSE file for more info.
